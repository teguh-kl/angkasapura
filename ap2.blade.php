 <!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Angkasa Pura</title>
<meta name="description" content="">

<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
<!--Style-->
<link rel="stylesheet" href="{{ config('app.url_cdn').'assets/partnership/ap2/css/reset.css' }}">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" href="{{ config('app.url_cdn').'assets/partnership/ap2/css/owl.carousel.css' }}">
<link rel="stylesheet" href="{{ config('app.url_cdn').'assets/partnership/ap2/css/owl.theme.default.min.css' }}">
<link rel="stylesheet" href="{{ config('app.url_cdn').'assets/partnership/ap2/css/style.css' }}">
<link rel="stylesheet" type="text/css" href="{{ config('app.url_cdn').'assets/partnership/ap2/css/media1024.css' }}"/>
<link rel="stylesheet" type="text/css" href="{{ config('app.url_cdn').'assets/partnership/ap2/css/media768.css' }}"/>
<link rel="stylesheet" type="text/css" href="{{ config('app.url_cdn').'assets/partnership/ap2/css/media480.css' }}"/>
<link rel="stylesheet" type="text/css" href="{{ config('app.url_cdn').'assets/partnership/ap2/css/media320.css' }}"/>
<!--link rel="stylesheet" href="css/style-temp.css"-->
<!--js-->

<script type="text/javascript" src="{{ config('app.url_cdn').'assets/partnership/ap2/js/vendor/jquery-1.9.1.min.js' }}"></script>
<script src="{{ config('app.url_cdn').'assets/partnership/ap2/js/vendor/modernizr-2.6.2.min.js' }}"></script>
<!--CDN link for  TweenMax-->
<script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/TweenMax.min.js"></script>
<script src="{{ config('app.url_cdn').'assets/partnership/ap2/js/owl.carousel.min.js' }}"></script>
<script src="{{ config('app.url_cdn').'assets/partnership/ap2/js/vendor/openWeather.js' }}"></script>
<script src="{{ config('app.url_cdn').'assets/partnership/ap2/js/jquery.infinitescroll.js' }}"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-96006190-22"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-96006190-22');
</script>

<!-- alexa -->
<script type="text/javascript">
_atrk_opts = {
    atrk_acct:"mXVUg1asOv00wn",
    domain:"liputan6.com",
    dynamic: true};
(function()
    { var as = document.createElement("script");
    as.type = "text/javascript"; as.async = true; as.src = "https://certify-js.alexametrics.com/atrk.js";
    var s = document.getElementsByTagName("script")[0];s.parentNode.insertBefore(as, s);
    }
)();
</script>

</head>
<body>
<!--[if lt IE 7]>
    <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
<![endif]--> 

<!-- header -->

<!-- end of header -->
<?php $page = "home"; ?>
<!-- middle -->
<div id="middle-content" class="homePage">
  <section id="mainBanner" class="section">
    <div id="weather_banner" class="bannerBig">
      <div class="temp_text">
        <h3 class="weather-temperature">0 &#x2103;</h3>
        <span><?php echo date("l\, jS F Y") . "<br>"; ?></span>
      </div><!--end.temp_text-->

      <p id="demo" style="display: none;"></p>
      <div class="place_text">
        <span>Selamat datang di,</span>
        <h3>Angkasa Pura II Free Wifi.</h3>
      </div>
    </div><!--end.bannerBig-->
  </section>
  <section id="list_tenant" class="section">
    <div class="wrapper">
      <div class="content_tenant">
        <div class="list_icon">
          <div class="item_icon">
            <a href="https://www.liputan6.com?utm_source=angkasapura2&utm_medium=wifi&utm_content=list-icon&utm_campaign=angkasapura2-wifi">
            <img src="{{ config('app.url_cdn').'assets/partnership/ap2/images/lip6.png' }}">
            </a>
          </div><!--end.item_icon-->
          <div class="item_icon">
            <a href="https://www.kapanlagi.com?utm_source=angkasapura2&utm_medium=wifi&utm_content=list-icon&utm_campaign=angkasapura2-wifi">
            <img src="{{ config('app.url_cdn').'assets/partnership/ap2/images/kapanlagi@2x.png' }}">
          </a>
          </div><!--end.item_icon-->
          <div class="item_icon">
            <a href="https://www.fimela.com?utm_source=angkasapura2&utm_medium=wifi&utm_content=list-icon&utm_campaign=angkasapura2-wifi">
            <img src="{{ config('app.url_cdn').'assets/partnership/ap2/images/fimela@2x.png' }}">
          </a>
          </div><!--end.item_icon-->
          <div class="item_icon">
            <a href="https://www.bola.com?utm_source=angkasapura2&utm_medium=wifi&utm_content=list-icon&utm_campaign=angkasapura2-wifi">
            <img src="{{ config('app.url_cdn').'assets/partnership/ap2/images/bolacom.png' }}">
          </a>
          </div><!--end.item_icon-->
          <div class="item_icon">
            <a href="https://www.bola.net?utm_source=angkasapura2&utm_medium=wifi&utm_content=list-icon&utm_campaign=angkasapura2-wifi">
            <img src="{{ config('app.url_cdn').'assets/partnership/ap2/images/bolanet.png' }}">
          </a>
          </div><!--end.item_icon-->
          <div class="item_icon">
            <a href="https://www.brilio.net?utm_source=angkasapura2&utm_medium=wifi&utm_content=list-icon&utm_campaign=angkasapura2-wifi">
            <img src="{{ config('app.url_cdn').'assets/partnership/ap2/images/brillio.png' }}">
          </a>
          </div><!--end.item_icon-->
          <div class="item_icon">
            <a href="https://www.dream.co.id?utm_source=angkasapura2&utm_medium=wifi&utm_content=list-icon&utm_campaign=angkasapura2-wifi">
            <img src="{{ config('app.url_cdn').'assets/partnership/ap2/images/dream.png' }}">
          </a>
          </div><!--end.item_icon-->
          <div class="item_icon">
            <a href="https://www.famous.id?utm_source=angkasapura2&utm_medium=wifi&utm_content=list-icon&utm_campaign=angkasapura2-wifi">
            <img src="{{ config('app.url_cdn').'assets/partnership/ap2/images/famous.png' }}">
          </a>
          </div><!--end.item_icon-->
          <div class="item_icon">
            <a href="https://www.merdeka.com?utm_source=angkasapura2&utm_medium=wifi&utm_content=list-icon&utm_campaign=angkasapura2-wifi">
            <img src="{{ config('app.url_cdn').'assets/partnership/ap2/images/merdeka.png' }}">
          </a>
          </div><!--end.item_icon-->
          <div class="item_icon">
            <a href="https://www.otosia.com?utm_source=angkasapura2&utm_medium=wifi&utm_content=list-icon&utm_campaign=angkasapura2-wifi">
            <img src="{{ config('app.url_cdn').'assets/partnership/ap2/images/otosia.png' }}">
          </a>
          </div><!--end.item_icon-->
        </div>
      </div><!--end.content_tenant-->
    </div><!--end,wrapper-->
  </section>
  <section id="main_content" class="section">
    <div class="wrapper">
      <div class="bride_text">
        <h3>Kamu Sudah Tau Belum?</h3>
        <p>Fakta menarik yang harus kamu tau sebelum liburan, Yuk simak disini!</p>
      </div><!--end.bride_text-->

      <div id="list-newsnya" class="list_news relative">
        <div class="loader_gif" style="display: block;">
          <div class="loadingnya">
            <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
          </div>
        </div>
      </div><!--end.list_news-->
      <div id="paging" data-url="">
      </div>
      <div class="loadCenter text-center" style="display: none;">
        <div class="lds-facebook"><div></div><div></div><div></div></div>
      </div>
    </div><!--end.wrapper-->
  </section>


  
</div>
<script src="{{ config('app.url_cdn').'assets/partnership/ap2/js/js_lib_v2.js' }}"></script>
<script src="{{ config('app.url_cdn').'assets/partnership/ap2/js/js_run.js' }}"></script>
<!-- comscore -->
<script type=text/javascript>function loadComscore(){var e=document.createElement("script");e.src=("https:"==document.location.protocol?"https://sb":"http://b")+".scorecardresearch.com/beacon.js";var c=document.querySelector('script[src="'+e.src+'"]');null!=c&&null!=c&&""!=c?(c.parentNode.removeChild(c),e.async=!1):e.async=!0;var o=document.getElementsByTagName("script")[0];o.parentNode.insertBefore(e,o)}var _comscore=_comscore||[];_comscore.push({c1:"2",c2:"15220176"}),loadComscore()</script>

<script type="text/javascript">
  loadAPI();
  changeBanner();
  setTimeout(function(){
    getLocation();
  }, 1000);
</script>
<footer>
  <div class="logo_footer">
    <img src="{{ config('app.url_cdn').'assets/partnership/ap2/images/logo_ap_kly.png' }}">
  </div>
</footer>
</body>
</html>